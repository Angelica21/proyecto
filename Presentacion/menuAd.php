<?php 
$administrador = new Administrador($_SESSION["id"]);
$administrador -> consultar();
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/sesionAd.php")?>"><i ><img src="img/logo.png" width=100px ></i></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dispositivos
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/dispositivos/crear.php")?>">Crear</a>
          <a class="dropdown-item" href="">Consultar</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="">Buscar</a>
        </div>
      </li>
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Empleado
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">insertar</a>
          <a class="dropdown-item" href="#">Consultar</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Ventas</a>
        </div>
      </li>
     
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Categoria
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="">Consultar</a>
        </div>
      </li>
      <li>  
      <form class = "form-inline my-2 my-lg-0">
      <input class = "form-control mr-sm-2" type = "text" placeholder = "Buscar">
      <button class = "btn btn-outline-secondary my-2 my-sm-0" type = "submit  vertical-align: heredar; "> Buscar</button>
    </form>
    </li>

    </ul>
	<ul class="navbar-nav">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Administrador: <?php echo $administrador -> getNombre() . " " . $administrador -> getApellido(); ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Editar Perfil</a>
          <a class="dropdown-item" href="#">Editar Foto</a>
        </div>
      </li>      
      <li class="nav-item">
        <a class="nav-link" href="index.php?sesion=0">Cerrar Sesión</a>
      </li>
      
    </ul>
	
  </div>
</nav>