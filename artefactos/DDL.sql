-- MySQL Script generated by MySQL Workbench
-- Tue Dec  1 14:39:01 2020
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema Proyectodb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Proyectodb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Proyectodb` DEFAULT CHARACTER SET utf8 ;
USE `Proyectodb` ;

-- -----------------------------------------------------
-- Table `Proyectodb`.`Cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Proyectodb`.`Cliente` (
  `idCliente` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `telefono` VARCHAR(45) NOT NULL,
  `correo` VARCHAR(45) NOT NULL,
  `clave` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idCliente`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Proyectodb`.`Carrito`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Proyectodb`.`Carrito` (
  `idCarrito` INT NOT NULL AUTO_INCREMENT,
  `Cliente_idCliente` INT NOT NULL,
  PRIMARY KEY (`idCarrito`, `Cliente_idCliente`),
  INDEX `fk_Carrito_Cliente_idx` (`Cliente_idCliente` ASC) VISIBLE,
  CONSTRAINT `fk_Carrito_Cliente`
    FOREIGN KEY (`Cliente_idCliente`)
    REFERENCES `Proyectodb`.`Cliente` (`idCliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Proyectodb`.`Categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Proyectodb`.`Categoria` (
  `idCategoria` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idCategoria`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Proyectodb`.`Dispositivo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Proyectodb`.`Dispositivo` (
  `idDispositivo` INT NOT NULL AUTO_INCREMENT,
  `marca` VARCHAR(45) NOT NULL,
  `modelo` VARCHAR(45) NOT NULL,
  `precio` VARCHAR(45) NOT NULL,
  `Categoria_idCategoria` INT NOT NULL,
  PRIMARY KEY (`idDispositivo`, `Categoria_idCategoria`),
  INDEX `fk_Dispositivo_Categoria1_idx` (`Categoria_idCategoria` ASC) VISIBLE,
  CONSTRAINT `fk_Dispositivo_Categoria1`
    FOREIGN KEY (`Categoria_idCategoria`)
    REFERENCES `Proyectodb`.`Categoria` (`idCategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Proyectodb`.`Carrito_dispositivo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Proyectodb`.`Carrito_dispositivo` (
  `idCarrito_dispositivo` INT NOT NULL AUTO_INCREMENT,
  `cantidad` INT NOT NULL,
  `Carrito_idCarrito` INT NOT NULL,
  `Dispositivo_idDispositivo` INT NOT NULL,
  PRIMARY KEY (`idCarrito_dispositivo`, `Carrito_idCarrito`, `Dispositivo_idDispositivo`),
  INDEX `fk_Carrito_producto_Carrito1_idx` (`Carrito_idCarrito` ASC) VISIBLE,
  INDEX `fk_Carrito_producto_Dispositivo1_idx` (`Dispositivo_idDispositivo` ASC) VISIBLE,
  CONSTRAINT `fk_Carrito_producto_Carrito1`
    FOREIGN KEY (`Carrito_idCarrito`)
    REFERENCES `Proyectodb`.`Carrito` (`idCarrito`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Carrito_producto_Dispositivo1`
    FOREIGN KEY (`Dispositivo_idDispositivo`)
    REFERENCES `Proyectodb`.`Dispositivo` (`idDispositivo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Proyectodb`.`Empleado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Proyectodb`.`Empleado` (
  `idEmpleado` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `correo` VARCHAR(45) NOT NULL,
  `clave` VARCHAR(45) NOT NULL,
  `cedula` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idEmpleado`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Proyectodb`.`Venta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Proyectodb`.`Venta` (
  `idVenta` INT NOT NULL AUTO_INCREMENT,
  `fecha` DATE NOT NULL,
  `Carrito_dispositivo_idCarrito` INT NOT NULL,
  `Carrito_dispositivo_Carrito_idCarrito` INT NOT NULL,
  `Carrito_dispositivo_Dispositivo_idDispositivo` INT NOT NULL,
  `Empleado_idEmpleado` INT NOT NULL,
  PRIMARY KEY (`idVenta`, `Carrito_dispositivo_idCarrito`, `Carrito_dispositivo_Carrito_idCarrito`, `Carrito_dispositivo_Dispositivo_idDispositivo`, `Empleado_idEmpleado`),
  INDEX `fk_Venta_Carrito_dispositivo1_idx` (`Carrito_dispositivo_idCarrito` ASC, `Carrito_dispositivo_Carrito_idCarrito` ASC, `Carrito_dispositivo_Dispositivo_idDispositivo` ASC) VISIBLE,
  INDEX `fk_Venta_Empleado1_idx` (`Empleado_idEmpleado` ASC) VISIBLE,
  CONSTRAINT `fk_Venta_Carrito_dispositivo1`
    FOREIGN KEY (`Carrito_dispositivo_idCarrito` , `Carrito_dispositivo_Carrito_idCarrito` , `Carrito_dispositivo_Dispositivo_idDispositivo`)
    REFERENCES `Proyectodb`.`Carrito_dispositivo` (`idCarrito_dispositivo` , `Carrito_idCarrito` , `Dispositivo_idDispositivo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Venta_Empleado1`
    FOREIGN KEY (`Empleado_idEmpleado`)
    REFERENCES `Proyectodb`.`Empleado` (`idEmpleado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Proyectodb`.`Administrador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Proyectodb`.`Administrador` (
  `idAdministrador` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `correo` VARCHAR(45) NOT NULL,
  `clave` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idAdministrador`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Proyectodb`.`LogAdministrador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Proyectodb`.`LogAdministrador` (
  `idLogAdministrador` INT NOT NULL AUTO_INCREMENT,
  `accion` VARCHAR(45) NOT NULL,
  `datos` TEXT NOT NULL,
  `hora` VARCHAR(45) NOT NULL,
  `fecha` VARCHAR(45) NOT NULL,
  `Administrador_idAdministrador` INT NOT NULL,
  PRIMARY KEY (`idLogAdministrador`, `Administrador_idAdministrador`),
  INDEX `fk_LogAdministrador_Administrador1_idx` (`Administrador_idAdministrador` ASC) VISIBLE,
  CONSTRAINT `fk_LogAdministrador_Administrador1`
    FOREIGN KEY (`Administrador_idAdministrador`)
    REFERENCES `Proyectodb`.`Administrador` (`idAdministrador`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
