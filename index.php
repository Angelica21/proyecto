<?php
session_start();
require "logica/Administrador.php";
$pagSinSesion = array(
    "presentacion/recuperarClave.php",
    "presentacion/autenticar.php"
);

if(isset($_GET["sesion"])&& $_GET["sesion"]==0){
  $_SESSION["id"]=null;
}


$pid=null;
if(isset($_GET["pid"])){
  $pid=base64_decode($_GET["pid"]);
}

?>

<!doctype html>
<html lang="es">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  
  
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" >
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" ></script>
    <link href="https://bootswatch.com/4/pulse/bootstrap.css"
    rel="stylesheet" />
    <link rel="stylesheet" href="/path/to/bootstrap.min.css">
    <link rel="stylesheet" href="cookiealert.css">

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" ></script>
    <script src="cookiealert-standalone.js"></script>

    <title>ELECTRONICS</title>
    <!-- icono arriba de la pagina -->
    <link rel="icon" type="image/png" href="img/loguito.png" >

  </head>
  <body>
    

 <?php 
if(isset($pid)){
    if(isset($_SESSION["id"])){
     include "presentacion/menuAd.php";
     
      include $pid;
   
    }else if (in_array($pid, $pagSinSesion)) {
     include $pid;
   } else {
     header("Location: index.php");
   }
    }else{
     include "presentacion/inicio.php";
    }
 ?>


   
    
  </body>
</html>