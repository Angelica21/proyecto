<?php
require "persistencia/DispositivoDAO.php";

class Dispositivo{
    private $idDispositivo;
    private $marca;
    private $modelo;
    private $precio;
    private $categoria;
    private $foto;

    private $conexion;
    private $DispositivoDAO;

    
    public function getIdDispositivo()
    {
        return $this->idDispositivo;
    }

    /**
     * @return string
     */
    public function getMarca()
    {
        return $this->marca;
    }


    public function getModelo()
    {
        return $this->modelo;
    }

    public function getPrecio()
    {
        return $this->precio;
    }

    public function getCategoria()
    {
        return $this->Categoria;
    }
    public function getFoto()
    {
        return $this->foto;
    }

    function dispositivo ($pIdDispositivo="", $pMarca="", $pModelo="", $pPrecio="", $pCategoria="", $pFoto="") {
        $this -> idDispositivo = $pIdDispositivo;
        $this -> marca = $pMarca;
        $this -> modelo = $pModelo;
        $this -> precio = $pPrecio;
        $this -> categoria = $pCategoria;
        $this -> foto = $pFoto;
        $this -> conexion = new Conexion();
        $this -> dispositivoDAO = new DispositivoDAO($pIdDispositivo,$pMarca,$modelo,$pPrecio,$pCategoria,$pFoto);        
    }
    
    function consultar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> dispositivoDAO -> consultar());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();
        $this -> marca= $resultado[0];
        $this -> modelo = $resultado[1];
        
    }

    function crear(){
       
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> dispositivoDAO -> crear());
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> dispositivoDAO -> consultarTodos());
        $this -> conexion -> cerrar();
        $dispositivos = array();
        while(($dispositivos = $this -> conexion -> extraer()) != null){
            array_push($dispositivos, new Dispositivo($resultado[0], $resultado[1], $resultado[2]),$resultado[3],$resultado[4],$resultado[5]);
        }
        return $dispositivos;
    }
    
    function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> dispositivoDAO -> editar());
        $this -> conexion -> cerrar();
        
    }

    function consultarPorPagina($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> dispositivoDAO -> consultarPorPagina($cantidad, $pagina));
        $this -> conexion -> cerrar();
        $dispositivo = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($dispositivo, new Dispositivo($resultado[0], $resultado[1], $resultado[2]),$resultado[3],$resultado[4],$resultado[5]);
        }
        return $dispositivo;
    }
    
    function consultarTotalRegistros(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> dispositivoDAO -> consultarTotalRegistros());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();        
        return $resultado[0];
    }
    
    
}

	

	

	

	
?>